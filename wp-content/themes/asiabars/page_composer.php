<?php
/*
Template Name: Page Composer
*/
get_header(); ?>

<?php if ( have_posts() ) : the_post(); ?>
	
<div id="page-wrapper" class="container">
	<div class="row">
		<div class="col-sm-8 col-md-9">
		
			<?php vwpc_render(); ?>
		</div>
		
		<div class="col-sm-4 col-md-3">
			
			<?php include('asiabars-sidebar.php'); ?>
			
		</div>
	</div>
</div>
<?php endif; ?>

<?php get_footer(); ?>