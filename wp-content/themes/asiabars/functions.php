<?php

function enqueue_child_styles() {
	wp_enqueue_style( 'childe-style', get_bloginfo( 'stylesheet_url' ) );
}
add_action( 'wp_enqueue_scripts', 'enqueue_child_styles', 99 );

