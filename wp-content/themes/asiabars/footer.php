				<footer id="footer">
					<div class="container">
						<div class="row">
							<div class="col-sm-4">
								<h3 class="widget-title">Category Guide</h3>
								<div class="footer-guide">
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/?s=bars">Bars</a>
									</div>
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/?s=restaurants">Restaurants</a>
									</div>
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/?s=nightclubs">Nightclubs</a>
									</div>
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/?s=rooftop+bars+restaurants">Rooftop bars & restaurants</a>
									</div>
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/?s=waterfront+bars+restaurants">Waterfront bars & restaurants</a>
									</div>
								
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/?s=cafes+specialty+coffee">Cafes & specialty coffee</a>
									</div>
								</div>
							</div>
							
							<div class="col-sm-6">
								<h3 class="widget-title">Location Guide</h3>
								<div class="footer-guide">
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/category/singapore/" class="guide-city">Singapore:</a>
										<a href="<?php echo home_url(); ?>/category/restaurants-singapore/">Restaurants</a>|
										<a href="<?php echo home_url(); ?>/category/bars-singapore/">Bars</a>|
										<a href="<?php echo home_url(); ?>/category/nightclubs-singapore/">Nightclub</a>|
										<a href="<?php echo home_url(); ?>/category/cafes-specialty-coffee-singapore/">Cafe</a>|
										<a href="<?php echo home_url(); ?>/category/rooftop-bars-restaurants-singapore/">Rooftop</a>|
										<a href="<?php echo home_url(); ?>/category/events-promotions-singapore/">Events</a>
									</div>
									
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/category/hong-kong" class="guide-city">Hong Kong:</a>
										<a href="<?php echo home_url(); ?>/category/restaurants-hong-kong//">Restaurants</a>|
										<a href="<?php echo home_url(); ?>/category/bars-hong-kong/">Bars</a>|
										<a href="<?php echo home_url(); ?>/category/nightclubs-hong-kong/">Nightclub</a>|
										<a href="<?php echo home_url(); ?>/category/cafes-specialty-coffee-hong-kong/">Cafe</a>|
										<a href="<?php echo home_url(); ?>/category/rooftop-bars-restaurants-hong-kong/">Rooftop</a>|
										<a href="<?php echo home_url(); ?>/category/events-promotions-hongkong//">Events</a>
									</div>
									
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/category/bangkok/" class="guide-city">Bangkok:</a>
										<a href="<?php echo home_url(); ?>/category/restaurants-bangkok/">Restaurants</a>|
										<a href="<?php echo home_url(); ?>/category/bars-bangkok/">Bars</a>|
										<a href="<?php echo home_url(); ?>/category/nightclubs-bangkok/">Nightclub</a>|
										<a href="<?php echo home_url(); ?>/category/cafes-specialty-coffee-bangkok/">Cafe</a>|
										<a href="<?php echo home_url(); ?>/category/rooftop-bars-restaurants-bangkok/">Rooftop</a>|
										<a href="<?php echo home_url(); ?>/category/events-promotions-bangkok/">Events</a>
									</div>
									
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/category/jakarta" class="guide-city">Jakarta:</a>
										<a href="<?php echo home_url(); ?>/category/restaurants-jakarta/">Restaurants</a>|
										<a href="<?php echo home_url(); ?>/category/bars-jakarta/">Bars</a>|
										<a href="<?php echo home_url(); ?>/category/nightclubs-jakarta/">Nightclub</a>|
										<a href="<?php echo home_url(); ?>/category/cafes-specialty-coffee-jakarta/">Cafe</a>|
										<a href="<?php echo home_url(); ?>/category/rooftop-bars-restaurants-jakarta/">Rooftop</a>|
										<a href="<?php echo home_url(); ?>/category/events-promotions-jakarta/">Events</a>
									</div>
									
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/category/bali" class="guide-city">Bali:</a>
										<a href="<?php echo home_url(); ?>/category/restaurants-bali/">Restaurants</a>|
										<a href="<?php echo home_url(); ?>/category/bars-bali/">Bars</a>|
										<a href="<?php echo home_url(); ?>/category/nightclubs-bali/">Nightclub</a>|
										<a href="<?php echo home_url(); ?>/category/cafes-specialty-coffee-bali/">Cafe</a>|
										<a href="<?php echo home_url(); ?>/category/rooftop-bars-restaurants-bali//">Rooftop</a>|
										<a href="<?php echo home_url(); ?>/category/events-promotions-bali/">Events</a>
									</div>
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/category/manila" class="guide-city">Manila:</a>
										<a href="<?php echo home_url(); ?>/category/restaurants-manila/">Restaurants</a>|
										<a href="<?php echo home_url(); ?>/category/bars-manila/">Bars</a>|
										<a href="<?php echo home_url(); ?>/category/nightclubs-manila/">Nightclub</a>|
										<a href="<?php echo home_url(); ?>/category/cafes-specialty-coffee-manila/">Cafe</a>|
										<a href="<?php echo home_url(); ?>/category/rooftop-bars-restaurants-manila/">Rooftop</a>|
										<a href="<?php echo home_url(); ?>/category/events-promotions-manila/">Events</a>
									</div>
									
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/category/shanghai" class="guide-city">Shanghai:</a>
										<a href="<?php echo home_url(); ?>/category/restaurants-shanghai/">Restaurants</a>|
										<a href="<?php echo home_url(); ?>/category/bars-shanghai/">Bars</a>|
										<a href="<?php echo home_url(); ?>/category/nightclubs-shanghai/">Nightclub</a>|
										<a href="<?php echo home_url(); ?>/category/cafes-specialty-coffee-shanghai/">Cafe</a>|
										<a href="<?php echo home_url(); ?>/category/rooftop-bars-restaurants-shanghai/">Rooftop</a>|
										<a href="<?php echo home_url(); ?>/category/events-promotions-shanghai/">Events</a>
									</div>
									
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/category/kuala-lumpur/" class="guide-city">Kuala lumpur:</a>
										<a href="<?php echo home_url(); ?>/category/restaurants-kuala-lumpur/">Restaurants</a>|
										<a href="<?php echo home_url(); ?>/category/bars-kuala-lumpur//">Bars</a>|
										<a href="<?php echo home_url(); ?>/category/nightclubs-kuala-lumpur/">Nightclub</a>|
										<a href="<?php echo home_url(); ?>/category/cafes-specialty-coffee-kuala-lumpur/">Cafe</a>|
										<a href="<?php echo home_url(); ?>/category/rooftop-bars-restaurants-kuala-lumpur/">Rooftop</a>|
										<a href="<?php echo home_url(); ?>/category/events-promotions-kuala-lumpur/">Events</a>
									</div>
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/category/sydney" class="guide-city">Sydney:</a>
										<a href="<?php echo home_url(); ?>/category/restaurants-sydney/">Restaurants</a>|
										<a href="<?php echo home_url(); ?>/category/bars-sydney/">Bars</a>|
										<a href="<?php echo home_url(); ?>/category/nightclubs-sydney/">Nightclub</a>|
										<a href="<?php echo home_url(); ?>/category/cafes-specialty-coffee-sydney/">Cafe</a>|
										<a href="<?php echo home_url(); ?>/category/rooftop-bars-restaurants-sydney/">Rooftop</a>|
										<a href="<?php echo home_url(); ?>/category/events-promotions-sydney/">Events</a>
									</div>
								</div>
							</div>
							
							<div class="col-sm-4">
							
							</div>
						</div>
					</div>

					<div class="copyright">
						<div class="container">
							<div class="row">
								<div class="col-sm-10 copyright-left">
									<a href="<?php echo home_url(); ?>/about/" class="footer-link"><strong>About</strong></a>
									<a href="<?php echo home_url(); ?>/contact-us/" class="footer-link"><strong>Contact Us</strong></a>
									<?php echo vw_get_option( 'copyright_text' ); ?>
								</div>
								<div class="col-sm-2 copyright-right">
									<a class="back-to-top" href="#top">&uarr;	<?php _e( 'Back to top', 'envirra' ) ?></a>
								</div>
							</div>
						</div>
					</div>
				</footer>
				
			</div> <!-- Off canvas body inner -->
		
		<?php wp_footer(); ?>
	</body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-72590471-1', 'auto');
  ga('send', 'pageview');

</script>
</html>
