			<div class="widget">
				<div class="social-media">
					<h3>Follow Us</h3>
					<?php //vw_render_site_social_icons(); ?>
					
					<a class="site-social-icon" href="https://www.facebook.com/asiabars" title="Facebook"><i class="icon-social-facebook"></i></a>
					<a class="site-social-icon" href="https://twitter.com/asiabars" title="Twitter"><i class="icon-social-twitter"></i></a>	
					<a class="site-social-icon" href="https://plus.google.com/+Asia-bars/posts" title="Google+"><i class="icon-social-gplus"></i></a>
					<a class="site-social-icon" href="http://www.pinterest.com/asiabars/" title="Pinterest"><i class="icon-social-pinterest"></i></a>
								
				</div>
			</div>
			<div class="widget" id="desktop-pixels-300x250">
				<div style="width:300;height:600;">
					<?php if( is_home() || is_front_page() ) { ?>
					<script language='JavaScript' type='text/javascript'>var randomstr = new String (Math.random());randomstr = randomstr.substring(1,8);var SID_Mobile = 575186765870;var SID_Tablet = 127190195859;var SID_PC = 127190195859;var pixelSID = SID_PC; var isMobile = 0; var hideIfMobile = 0; if(/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {isMobile=0;pixelSID=SID_Mobile;} else if(/iPad|IEMobile|Opera Mini/i.test(navigator.userAgent)) {isMobile=0;pixelSID=SID_Tablet;} else {isMobile=1;pixelSID=SID_PC;}if ((isMobile==1) && (hideIfMobile==1)) {} else {document.write ("<" + "script language='JavaScript' type='text/javascript' src='" + "http://pixel-my.pixelinteractivemedia.com/adj.php?ts=" + randomstr + "&amp;sid=" + pixelSID + "'><" + "/script>");}</script>
					<?php } else { ?>
					<!--ROS Script-->
					<script language='JavaScript' type='text/javascript'>var randomstr = new String (Math.random());randomstr = randomstr.substring(1,8);var SID_Mobile = 666139305876;var SID_Tablet = 667299105860;var SID_PC = 667299105860;var pixelSID = SID_PC; var isMobile = 0; var hideIfMobile = 0; if(/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {isMobile=0;pixelSID=SID_Mobile;} else if(/iPad|IEMobile|Opera Mini/i.test(navigator.userAgent)) {isMobile=0;pixelSID=SID_Tablet;} else {isMobile=1;pixelSID=SID_PC;}if ((isMobile==1) && (hideIfMobile==1)) {} else {document.write ("<" + "script language='JavaScript' type='text/javascript' src='" + "http://pixel-my.pixelinteractivemedia.com/adj.php?ts=" + randomstr + "&amp;sid=" + pixelSID + "'><" + "/script>");}</script>
					<?php } ?>
				</div>
			</div>
			<div class="widget">
				<div class="newsletter">
										
					<!-- Begin MailChimp Signup Form -->
					<div id="mc_embed_signup">
					<h3>Newsletter</h3>
					<p>Latest articles and highlighted events-emailed monthly.</p>
					<form action="http://asia-bars.us2.list-manage.com/subscribe/post?u=fe7b602cbfec2f9c4bd3e95cc&amp;id=cf04464083" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank">
						<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Your email address" required>
						<div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="submit" class="button"></div>
					</form>
					</div>
					<!--End mc_embed_signup-->

				</div>
			</div>
			
			<div class="widget">
				<div class="featured">
					<h3>Featured</h3>
					<div class="post-item">
					<?php $current_banner = $wpdb->get_results("SELECT  `title`,`url`,`sort_order`,`small_image` FROM `".$wpdb->prefix."featured` order by sort_order", ARRAY_A);
								if(isset($current_banner) && count($current_banner) > 0)  { 
									while(list($k, $row) = each($current_banner))  {
							?>
							<article class="post-box fly-in post-box-classic clearfix appeared">
								<div class="post-thumbnail-wrapper">
									<a href="<?php echo $row['url']; ?>" ><img src="<?php echo $row['small_image']; ?>"  class="img-responsive" alt="image4"></a>
								</div>
								<div class="post-box-inner">
									<div class="post-categories clearfix">
										<a href="<?php echo $row['url']; ?>"><?php echo $row['title']; ?></a>
									</div>
								</div>
							</article>
							

							<?php } } ?>
					
					
						
					</div>
				</div>
			</div>
			
			<div class="widget">
				<div class="ad">
				<?php echo adrotate_ad(3); ?>
					<!--<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/ad.jpg" class="img-responsive"> -->
				</div>
			</div>
			
			<div class="widget">
				<div class="featured">
					<h3>Upcoming Events</h3>
					<div class="post-item">
						<?php $current_banner = $wpdb->get_results("SELECT  `title`,`url`,`sort_order`,`small_image` FROM `".$wpdb->prefix."events` order by sort_order", ARRAY_A);
								if(isset($current_banner) && count($current_banner) > 0)  { 
									while(list($k, $row) = each($current_banner))  {
							?>
							<article class="post-box fly-in post-box-classic clearfix appeared">
								<div class="post-thumbnail-wrapper">
									<a href="<?php echo $row['url']; ?>" ><img src="<?php echo $row['small_image']; ?>"  class="img-responsive" alt="image4"></a>
								</div>
								<div class="post-box-inner">
									<div class="post-categories clearfix">
										<a href="<?php echo $row['url']; ?>"><?php echo $row['title']; ?></a>
									</div>
								</div>
							</article>
							

							<?php } } ?>
						
					</div>
				</div>
			</div>
			
			<div class="widget">
				<div class="ad">
				<?php echo adrotate_ad(4); ?>
					
				</div>
			</div>
			
			<div class="widget">
				<div class="featured">
				<h3>Cocktails of the Month</h3>
						<div class="post-item">
	
						
						
						<?php $current_banner = $wpdb->get_results("SELECT  `title`,`url`,`sort_order`,`small_image` FROM `".$wpdb->prefix."cocktail` order by sort_order", ARRAY_A);
								if(isset($current_banner) && count($current_banner) > 0)  { 
									while(list($k, $row) = each($current_banner))  {
							?>
							<article class="post-box fly-in post-box-classic clearfix appeared">
								<div class="post-thumbnail-wrapper">
									<a href="<?php echo $row['url']; ?>" ><img src="<?php echo $row['small_image']; ?>"  class="img-responsive" alt="image4"></a>
								</div>
								<div class="post-box-inner">
									<div class="post-categories clearfix">
										<a href="<?php echo $row['url']; ?>"><?php echo $row['title']; ?></a>
									</div>
								</div>
							</article>
							

							<?php } } ?>
						
						</div>

				</div>
			</div>