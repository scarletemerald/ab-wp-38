				<footer id="footer">
					<div class="container">
						<div class="row">
							<div class="col-sm-4">
								<h3 class="widget-title">Category Guide</h3>
								<div class="footer-guide">
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/category/bars/">Bars</a>
									</div>
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/category/restaurants-2/">Restaurants</a>
									</div>
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/category/nightclubs/">Nightclubs</a>
									</div>
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/category/rooftop-bars-restaurants/">Rooftop bars & restaurants</a>
									</div>
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/category/waterfront-bars-restaurants/">Waterfront bars & restaurants</a>
									</div>
								
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/cafes-specialty-coffee/">Cafes & specialty coffee</a>
									</div>
								</div>
							</div>
							
							<div class="col-sm-6">
								<h3 class="widget-title">Location Guide</h3>
								<div class="footer-guide">
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/category/singapore/">Singapore:</a>
										<a href="<?php echo home_url(); ?>/category/restaurants-singapore/">Restaurants</a>|
										<a href="<?php echo home_url(); ?>/category/bars-singapore/">Bars</a>|
										<a href="<?php echo home_url(); ?>/category/nightclubs-singapore/">Nightclub</a>|
										<a href="<?php echo home_url(); ?>/category/cafes-specialty-coffee-singapore/">Cafe</a>|
										<a href="<?php echo home_url(); ?>/category/rooftop-bars-restraurants-singapore/">Rooftop</a>|
										<a href="<?php echo home_url(); ?>/category/events-promotions-singapore/">Events</a>
									</div>
									
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/category/hong-kong">Hong Kong:</a>
										<a href="<?php echo home_url(); ?>/category/restaurants-hong-kong//">Restaurants</a>|
										<a href="<?php echo home_url(); ?>/category/bars-hong-kong/">Bars</a>|
										<a href="<?php echo home_url(); ?>/category/nightclubs-hong-kong/">Nightclub</a>|
										<a href="<?php echo home_url(); ?>/category/cafes-specialty-coffee-hong-kong/">Cafe</a>|
										<a href="<?php echo home_url(); ?>/category/rooftop-bars-restraurants-hong-kong/">Rooftop</a>|
										<a href="<?php echo home_url(); ?>/category/events-promotions-hongkong//">Events</a>
									</div>
									
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/category/bangkok/">Bangkok:</a>
										<a href="<?php echo home_url(); ?>/category/restaurants-bangkok/">Restaurants</a>|
										<a href="<?php echo home_url(); ?>/category/bars-bangkok/">Bars</a>|
										<a href="<?php echo home_url(); ?>/category/nightclubs-bangkok/">Nightclub</a>|
										<a href="<?php echo home_url(); ?>/category/cafes-specialty-coffee-bangkok/">Cafe</a>|
										<a href="<?php echo home_url(); ?>/category/rooftop-bars-restaurants-bangkok/">Rooftop</a>|
										<a href="<?php echo home_url(); ?>/category/events-promotions-bangkok/">Events</a>
									</div>
									
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/category/jakarta">Jakarta:</a>
										<a href="<?php echo home_url(); ?>/category/restaurants-jakarta/">Restaurants</a>|
										<a href="<?php echo home_url(); ?>/category/bars-jakarta/">Bars</a>|
										<a href="<?php echo home_url(); ?>/category/nightclubs-jakarta/">Nightclub</a>|
										<a href="<?php echo home_url(); ?>/category/cafes-specialty-coffee-jakarta/">Cafe</a>|
										<a href="<?php echo home_url(); ?>/category/rooftop-bars-restaurants-jakarta/">Rooftop</a>|
										<a href="<?php echo home_url(); ?>/category/events-promotions-jakarta/">Events</a>
									</div>
									
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/category/bali">bali:</a>
										<a href="<?php echo home_url(); ?>/category/restaurants-bali/">Restaurants</a>|
										<a href="<?php echo home_url(); ?>/category/bars-bali/">Bars</a>|
										<a href="<?php echo home_url(); ?>/category/nightclubs-bali/">Nightclub</a>|
										<a href="<?php echo home_url(); ?>/category/cafes-specialty-coffee-bali/">Cafe</a>|
										<a href="<?php echo home_url(); ?>/category/rooftop-bars-restraurants-bali/">Rooftop</a>|
										<a href="<?php echo home_url(); ?>/category/events-promotions-bali/">Events</a>
									</div>
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/category/manila">Manila:</a>
										<a href="<?php echo home_url(); ?>/category/restaurants-manila/">Restaurants</a>|
										<a href="<?php echo home_url(); ?>/category/bars-manila/">Bars</a>|
										<a href="<?php echo home_url(); ?>/category/nightclubs-manila/">Nightclub</a>|
										<a href="<?php echo home_url(); ?>/category/cafes-specialty-coffee-manila/">Cafe</a>|
										<a href="<?php echo home_url(); ?>/category/rooftop-bars-restraurants-manila/">Rooftop</a>|
										<a href="<?php echo home_url(); ?>/category/events-promotions-manila/">Events</a>
									</div>
									
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/category/shanghai">Shanghai:</a>
										<a href="<?php echo home_url(); ?>/category/restaurants-shanghai/">Restaurants</a>|
										<a href="<?php echo home_url(); ?>/category/bars-shanghai/">Bars</a>|
										<a href="<?php echo home_url(); ?>/category/nightclubs-shanghai/">Nightclub</a>|
										<a href="<?php echo home_url(); ?>/category/cafes-specialty-coffee-shanghai/">Cafe</a>|
										<a href="<?php echo home_url(); ?>/category/rooftop-bars-restraurants-shanghai/">Rooftop</a>|
										<a href="<?php echo home_url(); ?>/category/events-promotions-shanghai/">Events</a>
									</div>
									
									<div class="guide-group">
										<a href="<?php echo home_url(); ?>/category/kula+lumpur">Kula lumpur:</a>
										<a href="<?php echo home_url(); ?>/category/restaurants-kula+lumpur/">Restaurants</a>|
										<a href="<?php echo home_url(); ?>/category/bars-kula+lumpur/">Bars</a>|
										<a href="<?php echo home_url(); ?>/category/nightclubs-kula+lumpur/">Nightclub</a>|
										<a href="<?php echo home_url(); ?>/category/cafes-specialty-coffee-kula+lumpur/">Cafe</a>|
										<a href="<?php echo home_url(); ?>/category/rooftop-bars-restraurants-kula+lumpur/">Rooftop</a>|
										<a href="<?php echo home_url(); ?>/category/events-promotions-kula+lumpur/">Events</a>
									</div>
								</div>
							</div>
							
							<div class="col-sm-4">
							
							</div>
						</div>
					</div>

					<div class="copyright">
						<div class="container">
							<div class="row">
								<div class="col-sm-10 copyright-left">
									<a href="<?php echo home_url(); ?>/about/" class="footer-link"><strong>About</strong></a>
									<a href="<?php echo home_url(); ?>/contact-us/" class="footer-link"><strong>Contact Us</strong></a>
									<?php echo vw_get_option( 'copyright_text' ); ?>
								</div>
								<div class="col-sm-2 copyright-right">
									<a class="back-to-top" href="#top">&uarr;	<?php _e( 'Back to top', 'envirra' ) ?></a>
								</div>
							</div>
						</div>
					</div>
				</footer>
				
			</div> <!-- Off canvas body inner -->
		
		<?php wp_footer(); ?>
	</body>
</html>