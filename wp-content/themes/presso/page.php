<?php get_header(); ?>

<?php
$sidebar_position = get_post_meta( get_queried_object_id(), 'vw_page_sidebar_position', true );
$sidebar_position_class = '';
if ( 'left' == $sidebar_position ) {
	$sidebar_position_class = 'sidebar-left';
} else if( 'right' == $sidebar_position ) {
	$sidebar_position_class = 'sidebar-right';
} else {
	$sidebar_position_class = 'sidebar-hidden';
}
?>

<div id="page-wrapper" class="container <?php echo $sidebar_position_class; ?>">
	<div class="row">
		<div id="page-content" class="col-sm-8 col-md-9">
			<?php if (have_posts()) : ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'templates/page-title' ); ?>
					
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?>>
						<?php get_template_part( 'templates/post-formats/format', get_post_format() ); ?>
						<div class="post-content"><?php the_content(); ?></div>
					</article>
					
				<?php endwhile; ?>

				<?php get_template_part( 'templates/pagination' ); ?>

			

			<?php else : ?>

				<h2><?php _e('Not Found', 'envirra') ?></h2>

			<?php endif; ?>
		</div>

		<?php if ( 'left' == $sidebar_position || 'right' == $sidebar_position ) : ?>
		<aside id="page-sidebar" class="sidebar-wrapper col-sm-4 col-md-3">
			<div class="widget">
				<div class="social-media">
					<h3>Social Media</h3>
					<?php vw_render_site_social_icons(); ?>
				</div>
			</div>
			
			<div class="widget">
				<div class="newsletter">
					<h3>Newsletter</h3>
					<p>Get daily news updates by email. Subscribe to our newsletter</p>
					<form>
						<input type="email" class="form-control" placeholder="Your email address">
						<input type="submit" value="Subscribe">
					</form>
				</div>
			</div>
			
			<div class="widget">
				<div class="featured">
					<h3>Featured</h3>
					<div class="post-item">
						<article class="post-box fly-in post-box-classic clearfix appeared">
							<div class="post-thumbnail-wrapper">
								<img src="http://prototype/asiabars/wp-content/uploads/2013/11/image4-750x420.png" class="img-responsive" alt="image4">
							</div>
							<div class="post-box-inner">
								<div class="post-categories clearfix">
									<h3 class="title"><a href="#">Featured Post</a></h3>
								</div>
							</div>
						</article>
						
						<article class="post-box fly-in post-box-classic clearfix appeared">
							<div class="post-thumbnail-wrapper">
								<img src="http://prototype/asiabars/wp-content/uploads/2013/11/image3-750x420.png" class="img-responsive" alt="image4">
							</div>
							<div class="post-box-inner">
								<div class="post-categories clearfix">
									<h3 class="title"><a href="#">Featured Post</a></h3>
								</div>
							</div>
						</article>
						
						<article class="post-box fly-in post-box-classic clearfix appeared">
							<div class="post-thumbnail-wrapper">
								<img src="http://prototype/asiabars/wp-content/uploads/2013/11/image2-750x420.png" class="img-responsive" alt="image4">
							</div>
							<div class="post-box-inner">
								<div class="post-categories clearfix">
									<h3 class="title"><a href="#">Featured Post</a></h3>
								</div>
							</div>
						</article>
						
						<article class="post-box fly-in post-box-classic clearfix appeared">
							<div class="post-thumbnail-wrapper">
								<img src="http://prototype/asiabars/wp-content/uploads/2013/11/image1-750x420.png" class="img-responsive" alt="image4">
							</div>
							<div class="post-box-inner">
								<div class="post-categories clearfix">
									<h3 class="title"><a href="#">Featured Post</a></h3>
								</div>
							</div>
						</article>
					</div>
				</div>
			</div>
			
			<div class="widget">
				<div class="ad">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/ad.jpg" class="img-responsive">
				</div>
			</div>
			
			<div class="widget">
				<div class="featured">
					<h3>Local Events</h3>
					<div class="post-item">
						<article class="post-box fly-in post-box-classic clearfix appeared">
							<div class="post-thumbnail-wrapper">
								<img src="http://prototype/asiabars/wp-content/uploads/2013/11/image4-750x420.png" class="img-responsive" alt="image4">
							</div>
							<div class="post-box-inner">
								<div class="post-categories clearfix">
									<h3 class="title"><a href="#">Local Events</a></h3>
								</div>
							</div>
						</article>
						
						<article class="post-box fly-in post-box-classic clearfix appeared">
							<div class="post-thumbnail-wrapper">
								<img src="http://prototype/asiabars/wp-content/uploads/2013/11/image3-750x420.png" class="img-responsive" alt="image4">
							</div>
							<div class="post-box-inner">
								<div class="post-categories clearfix">
									<h3 class="title"><a href="#">Event Post</a></h3>
								</div>
							</div>
						</article>
						
						<article class="post-box fly-in post-box-classic clearfix appeared">
							<div class="post-thumbnail-wrapper">
								<img src="http://prototype/asiabars/wp-content/uploads/2013/11/image2-750x420.png" class="img-responsive" alt="image4">
							</div>
							<div class="post-box-inner">
								<div class="post-categories clearfix">
									<h3 class="title"><a href="#">Event Post</a></h3>
								</div>
							</div>
						</article>
						
						<article class="post-box fly-in post-box-classic clearfix appeared">
							<div class="post-thumbnail-wrapper">
								<img src="http://prototype/asiabars/wp-content/uploads/2013/11/image1-750x420.png" class="img-responsive" alt="image4">
							</div>
							<div class="post-box-inner">
								<div class="post-categories clearfix">
									<h3 class="title"><a href="#">Event Post</a></h3>
								</div>
							</div>
						</article>
					</div>
				</div>
			</div>
			
			<div class="widget">
				<div class="ad">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/ad.jpg" class="img-responsive">
				</div>
			</div>
			
			<div class="widget">
				<div class="featured">
					<div class="post-item">
						<article class="post-box fly-in post-box-classic clearfix appeared">
							<div class="post-thumbnail-wrapper">
								<img src="http://prototype/asiabars/wp-content/uploads/2013/11/image4-750x420.png" class="img-responsive" alt="image4">
							</div>
							<div class="post-box-inner">
								<div class="post-categories clearfix">
									<h3 class="title"><a href="#">Post</a></h3>
								</div>
							</div>
						</article>
						
						<article class="post-box fly-in post-box-classic clearfix appeared">
							<div class="post-thumbnail-wrapper">
								<img src="http://prototype/asiabars/wp-content/uploads/2013/11/image3-750x420.png" class="img-responsive" alt="image4">
							</div>
							<div class="post-box-inner">
								<div class="post-categories clearfix">
									<h3 class="title"><a href="#">Post</a></h3>
								</div>
							</div>
						</article>
						
						<article class="post-box fly-in post-box-classic clearfix appeared">
							<div class="post-thumbnail-wrapper">
								<img src="http://prototype/asiabars/wp-content/uploads/2013/11/image2-750x420.png" class="img-responsive" alt="image4">
							</div>
							<div class="post-box-inner">
								<div class="post-categories clearfix">
									<h3 class="title"><a href="#">Post</a></h3>
								</div>
							</div>
						</article>
						
						<article class="post-box fly-in post-box-classic clearfix appeared">
							<div class="post-thumbnail-wrapper">
								<img src="http://prototype/asiabars/wp-content/uploads/2013/11/image1-750x420.png" class="img-responsive" alt="image4">
							</div>
							<div class="post-box-inner">
								<div class="post-categories clearfix">
									<h3 class="title"><a href="#">Post</a></h3>
								</div>
							</div>
						</article>
					</div>
				</div>
			</div>
		</aside>
		<?php endif; ?>
	</div>
</div>

<?php get_footer(); ?>