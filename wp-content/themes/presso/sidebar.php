<div class="sidebar-inner">
<?php 
if( is_page() ){
	/* Page Sidebar */
	$page_sidebar = get_post_meta( get_the_id(), 'vw_page_sidebar', true );
	dynamic_sidebar( $page_sidebar );

} else {
	/* Blog Sidebar */
	$blog_sidebar = vw_get_option( 'blog_sidebar' );
	dynamic_sidebar( $blog_sidebar );
}
?>
</div>