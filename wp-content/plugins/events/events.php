<?php
/*
Plugin Name: Events Sidebar
Plugin URI: http://www.point-star.net
Description: To add/edit/delete Menu Page events
Version: 1
Author: point-star
Author URI: http://www.point-star.net
*/

add_action('admin_menu', 'event_menu');



function event_menu() {
  add_options_page('Menuevent Options', ' Events Sidebar', 'manage_options', 'events', 'events');

}

function events() {
  if (!current_user_can('manage_options'))  {
    wp_die( __('You do not have sufficient permissions to access this page.') );
  }



  echo '<div class="wrap">';
 
  	if(strlen($_GET['action']) > 0 && $_GET['action']=='save')
	{
		//save data in database
		$title = $_POST['title'];
		$url = $_POST['url'];
		$sort_order = $_POST['sort_order'];
		
		$arr = array();
		$arr['title'] = $title;
		$arr['url'] = $url;
		$arr['sort_order'] = $sort_order;
		
		global $wpdb;
		
		//upload thumb image file
		if(strlen(trim($_FILES['small_image']['name']))>0)
		{
		
			$file = $_FILES['small_image'];
			$upload_dir = wp_upload_dir();
			$promotion_file_name = 'event_'.rand().'_'.$file['name'];
			$save_path = $upload_dir['basedir'].'/events/' . $promotion_file_name;
			move_uploaded_file($file['tmp_name'], $save_path);
			//wp_upload_bits($_FILES["image"]["name"], null, file_get_contents($_FILES["image"]["tmp_name"]));

			$arr['small_image'] = $upload_dir['baseurl'].'/events/'.$promotion_file_name;

		}
		
		
		
		if($_POST['featured_id'] > 0)
		{
			//edit			
			$id = $_POST['featured_id'];
			
			$arr_where = array();
			$arr_where['id'] = $id;
							
			$wpdb->update($wpdb->prefix.'events',$arr,$arr_where);			
			echo 'Events contents updated successfully';
		}
		else
		{
			//add			
			$wpdb->insert($wpdb->prefix.'events',$arr,array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'));
			
			//update link url with new id
			$last_insert_id =$wpdb->insert_id;
			
			echo 'Events added successfully';
		}
		
		$upload_dir = wp_upload_dir();
		
		display_event();
			
	}
	else
	{
  		echo display_event();
	}

  
  echo '</div>';

}



function display_event()
{
	global $wpdb;
	
	//delete
	if(strlen($_GET['delete']) > 0)
	{
		$del_id = $_GET['delete'];
		$wpdb->query("DELETE FROM ".$wpdb->prefix."events WHERE id = ".$del_id);
		echo "Events Deleted Successfully";

		$upload_dir = wp_upload_dir();

	}
	
	//edit
	if(strlen($_GET['edit']) > 0 && $_GET['edit'] > 0)
	{
		display_event_form();
	}
	
	//add
	if(strlen($_GET['add']) > 0 && $_GET['add'] =='yes')
	{
		display_event_form();
	}
	
	
		//show all Cocktail 
		
		echo '<link type="text/css" rel="stylesheet" href="' . get_bloginfo('wpurl') . '/wp-content/plugins/events/css/style.css" />';
		echo '<div class="wrap">';
		echo '<div id="icon-edit-pages" class="icon32"><br /></div><h2>Manage events sidebar</h2>';
		echo '</div><br />';
		
		echo '<table id="showevent">';
		echo '<thead>';
		echo '<tr><th>Events</th>
			 <th>Image</th>		
			<th>URL</th>		 
			 <th>Edit</th>
			<th>Delete</th></thead>';
		echo '</tr><tbody>';
		echo '<tr><td colspan=6 alignt="right"><a href="'.get_bloginfo('wpurl').'/wp-admin/options-general.php?page=events&add=yes">Add New Events</a></td></tr>';
		
		//get locations from db
		$recipe_result = $wpdb->get_results("SELECT  `title` ,`id`,`small_image`,`url` FROM `".$wpdb->prefix."events` order by sort_order", ARRAY_A);
		
		if(isset($recipe_result) && count($recipe_result) > 0)
		{
			while(list($k, $row) = each($recipe_result))
			{
			
				$small_image = $row['small_image'];
				
				if(strlen(trim($small_image))>0)
				{
					$small_image = '<img src="'.$row['small_image'].'" width="150">';	
				}
				else
				{
					$small_image = '<img src="/wp-content/themes/redhouse/images/no_image.jpg" width="200">';
				}
				
				echo '<tr><td>'.$row['title'].'</td>
				
				<td>'.$small_image.'</td>
				<td>'.$row['url'].'</td>
				<td><a href="'.get_bloginfo('wpurl').'/wp-admin/options-general.php?page=events&edit='.$row['id'].'">Edit</a></td><td><a href="'.get_bloginfo('wpurl').'/wp-admin/options-general.php?page=events&delete='.$row['id'].'" onclick="return confirm(\'Are you sure to delete?\')">Delete</a></td></tr>';
			}
		}
		
		echo '</tbody>';
		echo '</table>';	
}

function display_event_form()
{
	global $wpdb;
	
	//check if edit
	if($_GET['edit'] > 0)
	{
		$edit_id = $_GET['edit'];
		
		//get current value from database
		$current_foodtype = $wpdb->get_results("SELECT  `title` ,`small_image`,`sort_order`,`id`,`url` FROM `".$wpdb->prefix."events` where id=".$edit_id, ARRAY_A);
			
			if(isset($current_foodtype) && count($current_foodtype) > 0)
			{
				while(list($k, $row) = each($current_foodtype))
				{
					$title = $row['title'];
					$small_image = $row['small_image'];
					$sort_order = $row['sort_order'];
					
					$url = $row['url'];
				}
			}
			$action = 'Update';		
	}
	else
	{
		//add
		$title = '';
		$url = '';
		$small_image = '';
		$sort_order = '';
		
		$edit_id = '0';
		$action = 'Add New';		
	}
	
	
		echo '<link type="text/css" rel="stylesheet" href="' . get_bloginfo('wpurl') . '/wp-content/plugins/events/css/style.css" />';
		
		//javascript
		
		echo '<script language="javascript">
			function validateForm(frm)
			{
				HasErrors = 0;
								
				if(frm.title.value.length < 1)
				{
					document.getElementById(\'span_title\').innerHTML = "Enter Title";
					HasErrors = 1;
				}
				else
				{
					document.getElementById(\'span_title\').innerHTML = "";
				}
				
				
				
				
				if(frm.small_image.value.length < 1 && frm.cocktail_id.value <= 0)
				{
					document.getElementById(\'span_image\').innerHTML = "Select Image";
					HasErrors = 1;
				}
				else
				{
					document.getElementById(\'span_image\').innerHTML = "";
				}
				
				
				if(frm.url.value.length < 1 && frm.cocktail_id.value <= 0)
				{
					document.getElementById(\'span_url\').innerHTML = "Enter the URL";
					HasErrors = 1;
				}
				else
				{
					document.getElementById(\'span_url\').innerHTML = "";
				}
				
				
				
				if(HasErrors == 0)
				{
					return true;
				}
				return false;
			}
		</script>';

		
		
		echo '<div class="wrap">';
		echo '<div id="icon-edit-pages" class="icon32"><br /></div><h2>'.$action.' Menu Page events</h2>';
		echo '</div><br />';
		
		//breadcrum
		echo '<a href="'.get_bloginfo('wpurl').'/wp-admin/options-general.php?page=events">Events</a> &raquo; '.$action.'<br><br>';
		
		
  
		echo '<form enctype="multipart/form-data" method="post" name="frmfoodtype" onSubmit="return validateForm(this)" action="'.get_bloginfo('wpurl').'/wp-admin/options-general.php?page=events&action=save">';
		echo '<table id="showevent">';
		echo '<tbody>';
		echo '<tr><td>Title:</th><td><input type="text" name="title" size="39" value="'.$title.'">&nbsp;<span id="span_title" style="color:red" name="span_title"></span></td></tr>';
		echo '<tr><td>URL:</th><td><input type="text" name="url" size="39" value="'.$url.'">&nbsp;<span id="span_url" style="color:red" name="span_url"></span></td></tr>';
		echo '<tr><td>Events :</th><td><input type="file" name="small_image" size="32">&nbsp;<span id="span_image" style="color:red" name="span_image"></span></td>';
		
		if(strlen(trim($small_image))>0)
		{
			echo '<tr><td colspan="2"><img src="'.$small_image.'" width="150"></td></tr>';
		}
		
		
		
		echo '<tr><td>Sort Order:</th><td><input type="text" name="sort_order" id="sort_order" size="39" value="'.$sort_order.'"></td></tr>';
		
		
		echo '<tr><td colspan="2"><input type="submit" name="btnsubmit" value="Submit">&nbsp;<input type="button" name="btncancel" 
		value="Cancel" onclick="window.location=\''.get_bloginfo('wpurl').'/wp-admin/options-general.php?page=events'.'\'";> </td>';
		echo '</tbody>';
		echo '</table>';
		
		//hidden fields
		echo '<input type="hidden" name="featured_id" value="'.$edit_id.'">';
		echo '</form>';	
		
		
	
	exit;
}

?>
