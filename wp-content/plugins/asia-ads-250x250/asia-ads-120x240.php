<?php
/*
Plugin Name: Asia-bars 250x250 Ad Widget
Plugin URI: http://www.rippledev.com 
Description: An advertising widget that displays a 250 x 250 Pixel Image with referal link in your sidebar
Version: 1.0.0.1
Author: Ripple Edge Development
Author URI: http://www.rippledev.com 
*/

$AsiabarsAds_plugin_url = trailingslashit( WP_PLUGIN_DIR.'/'. dirname ( plugin_basename(__FILE__) ) );

class AsiaBarsAd250 extends WP_Widget {
	
	function AsiaBarsAd250() {
		parent::WP_Widget(false, 'Asia-bars 250x250 Ad Widget');
	}
	
	function widget($args, $instance) {
		extract($args);
		$title250 = esc_attr($instance['title250']);
		$text250 = $instance['text250'];
		
		echo $before_widget;
		if($title250 !== '')
		{
			echo $before_title;
			echo $title250;
			echo $after_title;
		} 
		?><div class="preloading_background rounded" style="width:250px; height:250px; margin-top: 8px; padding: 3px; border:1px solid #cccccc;"><?php echo $text250; ?></div><?php
		echo $after_widget;
		echo "<br style='clear:both;'>";
	}
	
	function form($instance) {
		$title250 = esc_attr($instance['title250']);
		$text250 = $instance['text250'];
	?>	
			<p>
				<label for="<?php echo $this->get_field_id('title250'); ?>"><?php _e('Title'); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id('title250'); ?>" name="<?php echo $this->get_field_name('title250'); ?>" type="text" value="<?php echo $title250; ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('text250'); ?>"><?php _e('Ads Script (250px * 250px)'); ?></label>
				<textarea class="widefat" id="<?php echo $this->get_field_id('text'); ?>" rows="15"  name="<?php echo $this->get_field_name('text250'); ?>"><?php echo $text250; ?></textarea>
			</p>
		<?php
	}
	
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title250'] = strip_tags($new_instance['title250']);
		$instance['text250'] = $new_instance['text250']; 
		return $instance;
	}
}

add_action('widgets_init', create_function('', 'return register_widget("AsiaBarsAd250");')); 
?>