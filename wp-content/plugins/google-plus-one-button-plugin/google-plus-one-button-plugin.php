<?php
/*
Plugin Name: Google +1 Button Plugin
Plugin URI: http://allanjosephbatac.com/Wordpress-Google-PlusOne-Plugin/
Description: Google +1 Button - Add +1 to your pages to help your site stand out +1 buttons let people who love your content recommend it on Google search
Donate link: http://allanjosephbatac.com/blog/
Tags: Google +1 Button, Google plus one, plus one, +1, Google, Google +1, Google button
Author: AJ Batac
Version: 0.1
Requires at least: 2.0.2
Tested up to: 3.1.3
Stable tag: 9.9
Author URI: http://allanjosephbatac.com/blog/
*/

if( !function_exists('wp_gpone_settings') )
{
	function wp_gpone_settings ()
	{
		//add_menu_page(page_title, menu_title, capability, handle, [function], [icon_url]);
		add_menu_page("Google +1", "Google +1", 8, basename(__FILE__), "wp_gpone_opt");
	}
}

if ( !function_exists('wp_gpone_opt') )
{
	function wp_gpone_opt()
	{
	$plugin_url = ( WP_PLUGIN_URL . '/' . dirname ( plugin_basename ( __FILE__ ) ) ) .'/';
	
	?>
	<div class="wrap">
	<div id="icon-themes" class="icon32"></div>
	<h2><strong>Google +1 Button Settings</strong></h2>
	<div><img src="<?php echo $plugin_url; ?>icon.png" align="absmiddle"> Google + 1 Button Plugin: Add +1 to your pages to help your site stand out. <br>+1 buttons let people who love your content recommend it on Google search!</div>
	
	<?php	
	if(isset($_POST['wp_gpo_form_submit']))
	{
		echo '<div style="color:green;font-weight:bold;background:#FFC;padding:4px;margin:2px 0;">Your Google +1 Settings was saved successfully!</div>';
	}
	?>
	
	<fieldset>
	<form name="wp_gpo_option_form" method="post">
	
	<h2>Select button placement</h2>
	<select name="wp_gpone_align" id="wp_gpone_align">
		<option value="nb" <?php if ((get_option("wp_gpone_align") == "nb") || (!get_option("wp_gpone_align"))) echo ' selected'; ?>>None (Bottom)</option>
		<option value="nt" <?php if (get_option("wp_gpone_align") == "nt") echo 'selected'; ?>>None (Top)</option>
		<option value="tl" <?php if (get_option("wp_gpone_align") == "tl") echo 'selected'; ?>>Top Left</option>
		<option value="tr" <?php if (get_option("wp_gpone_align") == "tr") echo 'selected'; ?>>Top Right</option>
		<option value="bl" <?php if (get_option("wp_gpone_align") == "bl") echo 'selected'; ?>>Bottom Left</option>
		<option value="br" <?php if (get_option("wp_gpone_align") == "br") echo 'selected'; ?>>Bottom Right</option>
	</select>
	
	<h2>Button Size</h2>
	<div class="description">Determines the size and amount of social context next to the button</div>
	
	<p><input id="small" name="wp_gpone_layout" type="radio" value="small" <?php if (get_option("wp_gpone_layout") == "small") echo 'CHECKED'; ?>>
	<label for="small">Small (15px) <img src="<?php echo $plugin_url; ?>small.jpg" align="absmiddle"></label>
	</p>
	<br clear="all" />
	
	<p><input id="medium" name="wp_gpone_layout" type="radio" value="medium" <?php if (get_option("wp_gpone_layout") == "medium") echo 'CHECKED'; ?>>
	<label for="medium">Medium (20px) <img src="<?php echo $plugin_url; ?>medium.jpg" align="absmiddle"></label>
	</p>
	<br clear="all" />
	
	<p><input id="standard" name="wp_gpone_layout" type="radio" value="standard" <?php if (get_option("wp_gpone_layout") == "standard") echo 'CHECKED'; ?>>
	<label for="standard">Standard (24px) <img src="<?php echo $plugin_url; ?>standard.jpg" align="absmiddle"></label>
	</p>
	<br clear="all" />
	
	<p><input id="tall" name="wp_gpone_layout" type="radio" value="tall" <?php if (get_option("wp_gpone_layout") == "tall") echo 'CHECKED'; ?>>
	<label for="tall">Tall (60px) <img src="<?php echo $plugin_url; ?>tall.jpg" align="absmiddle"></label>
	</p>
	<br clear="all" />
		
	<br /><br />
	<div class="description">Finally, save it and enjoy your Google +1 button!</div>
	<br />
	<input type="submit" value="Save Google +1 Button" class="button-primary">
	<input type="hidden" name="wp_gpo_form_submit" value="true" />
	</form>
	<br />
	<br />
	
	</fieldset>
	
	</div>
	<br />
	<hr style="height:1px;color:#666;">
	<strong>Did this plugin help? If you like it and it is/was useful, please consider donating (Buy me a cup of coffee?) :) Many thanks!</strong></p>
	<p>
	<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
	<input type="hidden" name="cmd" value="_s-xclick">
	<input type="hidden" name="hosted_button_id" value="LP7GD3EXU4Q9Q">
	<input type="image" src="https://www.paypal.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
	<img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
	</form>
	</p>
	<br />
	<p>
	<a href="http://www.twitter.com/ajbatac"><img src="http://twitter-badges.s3.amazonaws.com/follow_me-c.png" alt="Follow ajbatac on Twitter"/></a>
	</p>
	<br />
	
	<?php
	}
}

if( !function_exists('wp_gpone_update') )
{
	function wp_gpone_update()
	{
		if(isset($_POST['wp_gpo_form_submit']))
		{	
			update_option("wp_gpone_align", $_POST['wp_gpone_align']);
			update_option("wp_gpone_layout", $_POST['wp_gpone_layout']);
		}
	}
}

if( !function_exists('wp_gpone_format') )
{
	function wp_gpone_format( $align )
	{
		if($align == 'left') { $margin = '5px 5px 5px 0'; }
		if($align == 'none') { $margin = '5px 0'; }
		if($align == 'right') { $margin = '5px 0 5px 5px'; }
		
		$layout = get_option("wp_gpone_layout");
		
		if($layout == 'small') {
			$layout = '<g:plusone size="small"></g:plusone>';
		}
		elseif ($layout == 'medium') {
			$layout = '<g:plusone size="medium"></g:plusone>';
		}
		elseif ($layout == 'tall') {
			$layout = '<g:plusone size="tall"></g:plusone>';
		}
		else {
			$layout = '<g:plusone></g:plusone>';
		}
		
		$output = '<!-- Google +1 Button Plugin -->
				   <div id="wp_gpone_button" style="margin:'.$margin.';float:'.$align.';">
				   <script type="text/javascript" src="http://apis.google.com/js/plusone.js"></script>'.
				   $layout
				   .'</div><!-- Google +1 Button Plugin -->';
		return $output;
	}
}

if ( !function_exists('wp_gpone') )
{
	function wp_gpone( $content )
	{
		if( !is_feed() && !is_page() && !is_archive() && !is_search() && !is_404() )
		{
			switch( get_option("wp_gpone_align") )
			{
				case 'tl': // Top Left
					return wp_gpone_format('left') . $content;
				break;
				
				case 'tr':
					return wp_gpone_format('right') . $content;
				break;
				
				case 'bl':
					return $content . wp_gpone_format('left');
				break;
				
				case 'br':
					return $content . wp_gpone_format('right');
				break;
				
				case 'nt': // None (Top)
					return wp_gpone_format('none') . $content;
				break;
				
				case 'nb': // None (Bottom)
					return $content . wp_gpone_format('none');
				break;
				
				default:
					return $content . wp_gpone_format('none');
			}
		}
		else
		{
			return $content;
		}
	}
}

add_filter('the_content', 'wp_gpone');
add_action('admin_menu', 'wp_gpone_settings');
add_action('init', 'wp_gpone_update');
?>