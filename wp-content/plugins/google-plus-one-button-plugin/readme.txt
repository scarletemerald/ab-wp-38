=== Plugin Name ===
Plugin Name: Wordpress Facebook Like Plugin
Plugin URI: http://allanjosephbatac.com/Wordpress-Facebook-Like-Plugin/
Description: Wordpress Facebook Like Plugin - Add a Facebook "Like" Button to your blog posts and increase your visitor engagement instantly! This plugin adds a simple and easy to use Facebook Like functionality with admin options to change the location of Facebook Like button, font, show faces, use the box with count as well as other cool and useful options. Visitors "liking" your content automatically posts them as shared item on their Facebook profile with a thumbnail of your site's image post. Pretty cool! The Like button lets a user share your content with friends on Facebook. When the user clicks the Like button on your site, a story appears in the user's friends' News Feed with a link back to your website.
Donate link: http://allanjosephbatac.com/blog/
Tags: facebook like plugin, wordpress facebook like, admin, custom, face book, Facebook, facebook like, Facebook like widget, Facebook Widget, fb, fb like, featured, featured posts, Like, page, plugin, Post, posts, wordpress like, facebook recommend, wordpress facebook recommend, facebook send button, facebook send
Author: AJ Batac
Version: 0.4
Requires at least: 2.0.2
Tested up to: 3.1.2
Stable tag: 9.9
Author URI: http://allanjosephbatac.com/blog/

== Description ==

= Google +1 Button Plugin =

The Wordpress Google +1 Button Plugin: Add +1 to your pages to help your site stand out +1 buttons let people who love your content recommend it on Google search!

== Installation ==

This section describes how to install the plugin and get it working.

1. Upload `google-plus-one-button-plugin.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Set it up. There should be a new menu link on your WordPress left menu. Select your Google +1 options and hit save.
4. There is no step 4. ;-)

== Frequently Asked Questions ==

= Where does it show? = 

It goes to the clicker's Google Profile (the visitor who clicked the button). Try it out yourself and visit your Google Profile page here: https://profiles.google.com/ It should now be under the +1 tab.

= Is the plugin free? =

Absolutely. I just pray to heaven everyday that someone out there buys me a coffee. ;)

= Why use this plugin? =

Simple. The Google +1 button helps your site stand out above the rest. +1 buttons let people who love your content recommend it on Google search. That's additional back link to your website. Cool huh? ;)

= How about support? =

Follow me at Twitter @ajbatac or send me a mention. I'll reply as soon as I get it.

== Changelog ==

= 0.1 =

Initial version

== Upgrade Notice ==

None at this time

== Screenshots ==

View it here: http://allanjosephbatac.com/Wordpress-Google-PlusOne-Plugin/

== Did you like it? ==

Did it help? If you like this plugin and find it useful, please consider donating (buy me a cup of coffee?) :)
Consider donating: http://allanjosephbatac.com/Wordpress-Google-PlusOne-Plugin/

And... *please*, *please* rate this plugin --&gt; :)  Thanks!

Follow me on Twitter @ajbatac
http://twitter.com/ajbatac