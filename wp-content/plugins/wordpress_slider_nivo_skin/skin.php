<?php
/**
Plugin Name: Nivo jQuery Slider Skin
Description: Integrate most popular jQuery slider plugin with WP slider plugin
Author: Muneeb
Version: 0.1

Copyright(c) 2012 Muneeb ur Rehman http://muneeb.me
**/

function m_ssp_register_nivo_skin( $skins ) {

	$path = plugin_dir_path( __FILE__ );

	$skins[] = array(
			'name' => __( 'Nivo Slider Skin' ),
			'path' => $path,
			'description' => 'Most popular jQuery slider plugin skin'
		);

	return $skins;

}

function m_ssp_nivo_skin_scripts() {

	if ( is_admin() ) return FALSE;

	$script_url = plugins_url( 'nivo-slider/jquery.nivo.slider.js', __FILE__ );

	wp_enqueue_script( 'jquery' );

	wp_enqueue_script( 'jquery-nivo-slider', $script_url );

}

function m_ssp_nivo_skin_styles() {

	if ( is_admin() ) return FALSE;

	$style_url = plugins_url( 'nivo-slider/nivo-slider.css', __FILE__ );

	$default_theme_style_url = plugins_url( 
							'nivo-slider/themes/default/default.css', 
							__FILE__ );

	$bar_theme_style_url = plugins_url( 
							'nivo-slider/themes/bar/bar.css', 
							__FILE__ );

	$light_theme_style_url = plugins_url( 
							'nivo-slider/themes/light/light.css', 
							__FILE__ );

	$dark_theme_style_url = plugins_url( 
							'nivo-slider/themes/dark/dark.css', 
							__FILE__ );

	$pascal_theme_style_url = plugins_url( 
							'nivo-slider/themes/pascal/pascal.css', 
							__FILE__ );

	wp_enqueue_style( 'nivo-slider', $style_url );
	wp_enqueue_style( 'nivo-slider-default-theme', $default_theme_style_url );
	wp_enqueue_style( 'nivo-slider-bar-theme', $bar_theme_style_url );
	wp_enqueue_style( 'nivo-slider-light-theme', $light_theme_style_url );
	wp_enqueue_style( 'nivo-slider-dark-theme', $dark_theme_style_url );
	wp_enqueue_style( 'nivo-slider-pascal-theme', $pascal_theme_style_url );
}

function m_ssp_nivo_skin_animation_options( $active_skin, $current_animation ) {

	if ( $active_skin == plugin_dir_path( __FILE__ ) )
		include 'animation_html_options.php';
}

function m_ssp_nivo_skin_theme_options( $slider_id ) {

	$active_skin = muneeb_ssp_get_slider_skin( $slider_id );

	if ( $active_skin !== plugin_dir_path( __FILE__ ) ) 
		return FALSE;

	$slider_options = muneeb_ssp_slider_options( $slider_id );

	if ( ! isset( $slider_options['nivo_slider_theme'] ) )
		$active_theme = 'default';
	else
		$active_theme = $slider_options['nivo_slider_theme'];

	include 'theme_option_html.php';

}

function m_ssp_nivo_skin_add_theme_option( $slider_options ) {

	if ( ! isset( $slider_options['nivo_slider_theme'] ) )
		$slider_options['nivo_slider_theme'] = 'theme-default';

	return $slider_options;

}

//Add this skin to the slider skins option array
add_filter( 'ssp_skins_array', 'm_ssp_register_nivo_skin' );

//Add the JS scripts required for the skin to work properly
add_action( 'init', 'm_ssp_nivo_skin_scripts' );

//Add the CSS required by the skin
add_action( 'init', 'm_ssp_nivo_skin_styles' );

//Add the extra animation options or effects
add_action( 'ssp_html_option_animation_select', 
			'm_ssp_nivo_skin_animation_options', 10, 2 );

//Add the theme option in options metabox for slider post type
add_action( 'ssp_options_before_control_option', 'm_ssp_nivo_skin_theme_options' );

//Add and set the theme option to default if it does not exist in slider options.
add_action( 'ssp_before_save_slider_options', 'm_ssp_nivo_skin_add_theme_option' );

//The Nivo Pascal theme requires the image size to be exactly 630px x 235px
add_image_size( 'ssp_nivo_pascal_theme', 630, 235, true );

?>