<option value="sliceDown"
<?php selected( "sliceDown", $current_animation ) ?>>sliceDown</option>
<option value="sliceDownLeft"
<?php selected( "sliceDownLeft", $current_animation ) ?>>sliceDownLeft</option>
<option value="sliceUp"
<?php selected( "sliceUp", $current_animation ) ?>>sliceUp</option>
<option value="sliceUpLeft"
<?php selected( "sliceUpLeft", $current_animation ) ?>>sliceUpLeft</option>
<option value="sliceUpDown"
<?php selected( "sliceUpDown", $current_animation ) ?>>sliceUpDown</option>
<option value="sliceUpDownLeft"
<?php selected( "sliceUpDownLeft", $current_animation ) ?>>sliceUpDownLeft</option>
<option value="fold"
<?php selected( "fold", $current_animation ) ?>>Fold</option>
<option value="slideInLeft"
<?php selected( "slideInLeft", $current_animation ) ?>>slideInLeft</option>
<option value="boxRandom"
<?php selected( "boxRandom", $current_animation ) ?>>boxRandom</option>
<option value="boxRain"
<?php selected( "boxRain", $current_animation ) ?>>boxRain</option>
<option value="boxRainReverse"
<?php selected( "boxRainReverse", $current_animation ) ?>>boxRainReverse</option>
<option value="boxRainGrow"
<?php selected( "boxRainGrow", $current_animation ) ?>>boxRainGrow</option>
<option value="boxRainGrowReverse"
<?php selected( "boxRainGrowReverse", $current_animation ) ?>>boxRainGrowReverse</option>
<option value="random"
<?php selected( "random", $current_animation ) ?>>Random</option>