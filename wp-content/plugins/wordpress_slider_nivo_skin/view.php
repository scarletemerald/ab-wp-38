<div class="slider-wrapper nivo-slider-skin" id="slider_cont_<?php echo esc_attr( $slider_id ) ?>">
	<!--<div class="ribbon">
        </div> save for next versions-->
	<div id="slider_<?php echo esc_attr( $slider_id ) ?>" class="nivoSlider slides">
		<?php foreach( $slides as $slide ): ?>
			<?php if ( $slider_settings['linkable'] ): ?>
				<a href="<?php echo $slide['image']['link'] ?>">
					<?php if ( $slider_settings['caption_box'] ): ?>
						<img class="slide_image" src="<?php echo $slide['image']['url'] ?>" data-thumb="<?php echo $slide['image']['sizes']['thumbnail'] ?>" title="<?php echo esc_attr( $slide['image']['caption'] ) ?>" />
					<?php else: ?>
							<img class="slide_image" src="<?php echo $slide['image']['url'] ?>" data-thumb="<?php echo $slide['image']['sizes']['thumbnail'] ?>" />
					<?php endif; ?>
				</a>
			<?php else: ?>
				<?php if ( $slider_settings['caption_box'] ): ?>
					<img class="slide_image" src="<?php echo $slide['image']['url'] ?>" data-thumb="<?php echo $slide['image']['sizes']['thumbnail'] ?>" title="<?php echo esc_attr( $slide['image']['caption'] ) ?>" />
				<?php else: ?>
					<img class="slide_image" src="<?php echo $slide['image']['url'] ?>" data-thumb="<?php echo $slide['image']['sizes']['thumbnail'] ?>" />
				<?php endif; ?>
			<?php endif; ?>
		<?php endforeach; ?>
	</div>
</div>

<script>
	jQuery(function ($) {
		
		$(window).load(function() {
			id = "<?php echo esc_js( $slider_id ) ?>";
	        
	        options = <?php echo json_encode( $slider_settings ) ?>;

	        selector = $( '#slider_' + id );

	        slider_cont = $( '#slider_cont_' + id );

	        if ( ! options.nivo_slider_theme )
	        	slider_cont.addClass( 'theme-default' );
	        else
	        	slider_cont.addClass(options.nivo_slider_theme);

	        height = options.height.replace(/[^\d.]/g, "");

	        width = options.width.replace(/[^\d.]/g, "");

	        if ( options.h_responsive == false || options.h_responsive == '' ) {
	          
	          $('.slide_image', selector).each( function() {

	            if ( ! Number( height ) <= 0 )
	             $(this).css( 'height', height + 'px' );

	          });
	          
	        }

	        if ( options.w_responsive == false || options.w_responsive == '' ) {
	          
	          $('.slide_image', selector).each( function() {

	            if ( ! Number( width ) <= 0 )
	             $(this).css( 'width', width + 'px' );

	          });
	          
	        }



	        if ( options.animation == 'slide' )
	        	options.animation = 'slideInRight';

	        if ( ! options.slideshow )
	        	options.slideshow = true;
	        else
	        	options.slideshow = false;

	        selector.nivoSlider({

	        	effect: options.animation,

	        	animSpeed: options.animation_speed * 1000,

	        	pauseTime: options.cycle_speed * 1000,

	        	directionNav: options.direction_nav,

	        	controlNav: options.control_nav,

	        	pauseOnHover: options.pause_on_hover,

	        	manualAdvance: options.slideshow,

	        	controlNavThumbs: options.thumbnail_navigation,

	        });

    	});
});
</script>