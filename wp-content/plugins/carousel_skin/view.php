<div class="flexslider ssp_carousel_skin ssp ssp_slider_default" id="slider_<?php echo esc_attr( $slider_id ) ?>">
	<ul class="slides ssp_slider wsp_default_skin">
		<?php foreach( $slides as $slide ): ?>
			<li class="c_slide">
				<?php if ( $slider_settings['linkable'] ): ?>
					<a href="<?php echo $slide['image']['link'] ?>">
						<img class="slide_image" src="<?php echo $slide['image']['url'] ?>" />
					</a>
				<?php else: ?>
					<img class="slide_image" src="<?php echo $slide['image']['url'] ?>" />
				<?php endif ?>
			</li>
		<?php endforeach ?>
	</ul>
</div>

<script>
	jQuery(function ($) {

        $(window).load(function() {
		
  		    id = "<?php echo esc_js( $slider_id ) ?>";
          
          options = <?php echo json_encode( $slider_settings ) ?>;

          selector = $( '#slider_' + id );

          $('.slides', selector).css('margin', 0);

          height = options.height.replace(/[^\d.]/g, "");

          width = options.width.replace(/[^\d.]/g, "");

          if ( options.h_responsive == false || options.h_responsive == '' ) {
            
            $('.slides .slide_image', selector).each( function() {

              if ( ! Number( height ) <= 0 )
               $(this).css( 'height', height + 'px' );

            });
            
          }

          if ( options.w_responsive == true )
          	width = 210;

          $(selector).flexslider( {

            smoothHeight: options.h_responsive,

            animation: 'slide',

            slideshow: options.slideshow,

            slideshowSpeed: Number( options.cycle_speed ) * 1000,

            animationSpeed: Number( options.animation_speed ) * 1000,

            pauseOnHover: options.pause_on_hover,

            controlNav: options.control_nav,

            directionNav: options.direction_nav,

            keyboard: options.keyboard_nav,

            touch: options.touch_nav,

            itemWidth: width,

            itemMargin: 5,

            minItems: 1

          });

        });
    });
</script>