<?php
/*
Plugin Name: Featured-Post-List-Widget
Plugin URI: http://www.rippledev.com 
Description: Shows a list of featured bars using the featured-post-list
Version: 1.0.0.1
Author: Ripple Edge Development
Author URI: http://www.rippledev.com 
*/

$FeaturedPostWidget = trailingslashit( WP_PLUGIN_DIR.'/'. dirname ( plugin_basename(__FILE__) ) );

class FeaturedPostWidget extends WP_Widget {
	
	function FeaturedPostWidget() {
		parent::WP_Widget(false, 'FeaturedPostWidget');
	}
	
	function widget($args, $instance) {
		extract($args);
		$title240 = esc_attr($instance['title240']);
		$text240 = $instance['text240'];
		
		echo $before_widget;
		if($title240 !== '')
		{
			echo $before_title;
			echo $title240;
			echo $after_title;
		} 
		?>
		<div id="featured_label_bg">Featured</div>
		<div id="featured_content" class="preloading_background rounded">
			<ol id="featured_ol">
				<?php featuredpostsList(); ?>
			</ol>
		</div><?php
		echo $after_widget;
		echo "<br style='clear:both;'>";
	}
	
	function form($instance) {
		
	}
	
	function update($new_instance, $old_instance) {

	}
}

add_action('widgets_init', create_function('', 'return register_widget("FeaturedPostWidget");')); 
?>