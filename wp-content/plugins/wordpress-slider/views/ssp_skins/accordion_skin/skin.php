<?php
/**
Plugin Name: Accordion Image Slider Skin
Description: Horizontal accordion slider skin for WP slider plugin
Author: Muneeb
Version: 0.1

Copyright(c) 2012 Muneeb ur Rehman http://muneeb.me
**/

function m_ssp_register_accordion_skin( $skins ) {

	$path = plugin_dir_path( __FILE__ );

	$skins[] = array(
			'name' => __( 'Accordion Image Slider Skin' ),
			'path' => $path,
			'description' => 'Horizontal accordion image slider skin'
		);

	return $skins;

}

function m_ssp_accordion_skin_scripts() {

	if ( is_admin() ) return FALSE;

	$script_url = plugins_url( 'js/jquery.zaccordion.min.js', __FILE__ );

	wp_enqueue_script( 'jquery' );

	wp_enqueue_script( 'jquery-zaccordion', $script_url );

}

function m_ssp_accordion_skin_wp_head() {

	if ( is_admin() ) return FALSE;

	$path = plugin_dir_path( __FILE__ );

	$style_path = $path . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR 
						. 'style.css';

	echo '<style>';

	include $style_path;
	
	echo '</style>';

}

//Add this skin to the slider skins option array
add_filter( 'ssp_skins_array', 'm_ssp_register_accordion_skin' );

//Add the JS scripts required for the plugin to work
add_action( 'init', 'm_ssp_accordion_skin_scripts' );

//Add the scripts and styles required for styling/functionality in the head element
add_action( 'wp_head', 'm_ssp_accordion_skin_wp_head' );

?>