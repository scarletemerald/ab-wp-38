<div class="accordion_skin" id="slider_<?php echo esc_attr( $slider_id ) ?>">
	<div class="a_loading_image">
		<img src="<?php echo plugins_url('images/loading.gif', __FILE__ ) ?>" />
	</div>
	<ul class="ssp_slider slides">
		<?php foreach( $slides as $slide ): ?>
			<li class="slide">
				<img class="slide_image" src="<?php echo $slide['image']['url'] ?>" />
				<?php if ( $slider_settings['caption_box'] ): ?>
					<div class="caption_box">
						<?php if ( $slider_settings['linkable'] ): ?>
							<a class="slide_link" href="<?php echo $slide['image']['link'] ?>">
								<strong>
									<?php echo $slide['image']['title'] ?>
								</strong>
								<p>
									<?php echo $slide['image']['caption'] ?>
								</p>
							</a>
						<?php else: ?>
							<strong>
								<?php echo $slide['image']['title'] ?>
							</strong>
							<p>
								<?php echo $slide['image']['caption'] ?>
							</p>
						<?php endif; ?>
					</div>
				<?php endif; ?>
			</li>
		<?php endforeach; ?>
	</ul>
</div>

<script>
	jQuery(function ($) {
		
		id = "<?php echo esc_js( $slider_id ) ?>";
        
        options = <?php echo json_encode( $slider_settings ) ?>;

        selector = $( '#slider_' + id );

        height = options.height.replace(/[^\d.]/g, "");

        width = options.width.replace(/[^\d.]/g, "");

        cont_height = '100%';

        cont_width = '100%';

        tab_width = '10%';

        if ( options.h_responsive == false || options.h_responsive == '' ) {
          
          $('.slides .slide_image', selector).each( function() {

            if ( ! Number( height ) <= 0 )
             $(this).css( 'height', height + 'px' );

          });

          if ( Number( height ) > 0 )
          	cont_height = height;
          
        }

        if ( options.w_responsive == false || options.w_responsive == '' ) {
          
          if ( Number( width ) > 0 ) {

          	cont_width = width;
          	tab_width = 100;
          	
          }
          
        }
       
		$(".ssp_slider", selector).zAccordion({

			auto: options.slideshow,

			speed: options.animation_speed * 1000,

			timeout: options.cycle_speed * 1000,

			pause: options.pause_on_hover,
			
			tabWidth: tab_width,
			
			slideClass: 'slider',

			animationStart: function () {
				$('.accordion_skin .ssp_slider', selector).find('li.slider-open div').css('display', 'none');
				$('.accordion_skin .ssp_slider', selector).find('li.slider-previous div').css('display', 'none');
			},
			animationComplete: function () {
				$('.accordion_skin .ssp_slider', selector).find('li div').fadeIn(600);
			},

			buildComplete: function () {
				$('.a_loading_image', selector).hide();
			},

			width: cont_width,

			height: cont_height
			
		});
		
	});
</script>
