<?php

/*=== WordPress Admin Options Page ===*/
/* By ilovecolors.com.ar */

add_action('contextual_help', 'ilc_plus_one_contextual_help');

function ilc_plus_one_contextual_help($contextual_help) {
	
	$screen = $_GET['page'];
	
	if ($screen == plugin_basename(dirname(__FILE__).'/ilc-admin.php')) {
		$contextual_help = "<h5>ILC Plus One by ilovecolors.com.ar</h5>";
		$contextual_help .= "<p>The first three settings are the same than those that can be found in the <a href='http://code.google.com/apis/+1button/' target='_blank'>+1 code builder page</a>. The last two settings define the position of the button in entries and pages your WordPress site:</p>
		<ul><li><p><strong>Button Placement in Posts/Pages</strong>: Choose if you want to display the button at the top of each post or page or at the bottom. <small>Default value: After content.</small></p></li>
		<li><p><strong>Align left or right</strong>: This will add a float:left or float:right to the style of the wrapping div for the button to display your button aligned to right, left or without any alignment. By default no style is outputted. <small>Default value: None.</small></p></li>
		</ul>
		<p>The code outputted is wrapped in a <strong>.ilc-plus-one</strong> so you can use it instead to stylize the position of your button.</p>
		<p>If you find it useful, you could <a href='http://twitter.com/share?text=Add%20Google%20%2B1%20to%20your%20WordPress%20blog%20with%20this%20plugin!&url=http%3A%2F%2Fwww.ilovecolors.com.ar%2Fwordpress-plugin-add-google-1%2F' target='_blank'>share it</a> with others and show some love! You can <a href='http://twitter.com/eliorivero' target='_blank'>Follow me on Twitter</a> for daily doses of WordPress, Design, Typography and jQuery. Thanks!</p>
		
		
		
		";
	}
	return $contextual_help;
}

function ilc_plus_one_options(){
	$stored_ilc_plus_one = get_option('stored_ilc_plus_one');
	$name_ilc_plus_one = array(
		'lang' => array(
			'Arabic' => 'ar',
			'Bulgarian' => 'bg',
			'Catalan' => 'ca',
			'Chinese (Simplified)' => 'zh-CN',
			'Chinese (Traditional)' => 'zh-TW',
			'Croatian' => 'hr',
			'Czech' => 'cs',
			'Danish' => 'da',
			'Dutch' => 'nl',
			'English (UK)' => 'en-GB',
			'English (US)' => '',
			'Estonian' => 'et',
			'Filipino' => 'fil',
			'Finnish' => 'fi',
			'French' => 'fr',
			'German' => 'de',
			'Greek' => 'el',
			'Hebrew' => 'iw',
			'Hindi' => 'hi',
			'Hungarian' => 'hu',
			'Indonesian' => 'id',
			'Italian' => 'it',
			'Japanese' => 'ja',
			'Korean' => 'ko',
			'Latvian' => 'lv',
			'Latin American' => 'es-419',
			'Portuguese (Brasil)' => 'pt-BR',
			'Portuguese (Portugal)' => 'pt-PT',
			'Spanish' => 'es',
			'Russian' => 'ru',
		),
		'size' => '',
		'count' => true,
		'track' => true,
		'place' => 'after',
		'float' => 'none'
	);	
	foreach($name_ilc_plus_one as $key => $val){
		$name[$key] = $key;
		$value[$key] = $stored_ilc_plus_one[$key];
	}

	if($_POST['ilc_plus_one_submit'] == 'submit') {
		echo '<div class="updated"><p><strong>+1 button settings saved.</strong></p></div>';
		foreach($name_ilc_plus_one as $key => $val){
			$stored_ilc_plus_one[$key] = $_POST[$name[$key]];
			$value[$key] = $_POST[$name[$key]];
		}
		update_option('stored_ilc_plus_one', $stored_ilc_plus_one);
	}

?>
	
<div class="wrap">
<h2><?php _e('ILC Plus One Settings', 'ilc'); ?></h2>

<p><?php _e('Set your options below. For help, pull the contextual <em>Help</em> tab at the top.', 'ilc'); ?></p>

<form id="ilc_plus_one_settings" method="post" action="<?php echo esc_url(str_replace( '%7E', '~', $_SERVER['REQUEST_URI'])); ?>">
<input type="hidden" name="ilc_plus_one_submit" value="submit" />

<table class="form-table">
	<tr>
		<th scope="row">
			<h3><?php _e('Select language', 'ilc'); ?></h3>
		</th>
				
		<td>
			<table>
				<tr>
					<td>
						<select name="<?php echo $name['lang']; ?>" id="lang">
							<?php foreach ($name_ilc_plus_one['lang'] as $key => $val) { ?>
							<option value="<?php echo $val; ?>" <?php if ($value['lang'] == $val) echo 'selected="selected"'; ?>>
								<?php echo $key; ?>
							</option>
							<?php } ?>
						</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<th scope="row">
			<h3><?php _e('Track +1 vote', 'ilc'); ?></h3>
		</th>
		<td>
			<table>
			<tr>
				<td>
					<label for="<?php echo $name['track']; ?>">
					<input id="<?php $name['track']; ?>" name="<?php echo $name['track']; ?>" type="checkbox" <?php if ( $value['track'] ) echo 'checked="checked"'; ?> value="true" /> 
							<?php _e( 'Tracks vote in Google Analytics as <a href="http://yoast.com/plus-one-google-analytics/" title="Tracking Google +1 button Interaction in Google Analytics" target="_blank">described by Yoast</a>.', 'ilc' ); ?></label>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<th scope="row">
			<h3><?php _e('Include count', 'ilc'); ?></h3>
		</th>
		<td>
			<table>
			<tr>
				<td>
					<label for="<?php echo $name['count']; ?>">
					<input id="<?php $name['count']; ?>" name="<?php echo $name['count']; ?>" type="checkbox" <?php if ( $value['count'] ) echo 'checked="checked"'; ?> value="true" /> 
							<?php _e( 'Yes, show total count.', 'ilc' ); ?></label>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<th scope="row">
			<h3><?php _e('Button Size:', 'ilc'); ?></h3>
		</th>
		<td >
			<table class="ilc-subtable">
				<tr>
					<td>
						<?php echo '<img src="' . plugins_url( 'standard.gif' , __FILE__ ) . '" > '; ?><br/>
						<input id="standard" name="<?php echo $name['size']; ?>" type="radio" <?php if ( $value['size'] == "") echo 'checked="checked"'; ?> value="" />
						<label for="standard"><?php _e( 'Standard (default).', 'ilc' ); ?></label>
					</td>
		
					<td>
						<?php echo '<img src="' . plugins_url( 'medium.gif' , __FILE__ ) . '" > '; ?><br/>
						<input id="medium" name="<?php echo $name['size']; ?>" type="radio" <?php if ( $value['size'] == "medium") echo 'checked="checked"'; ?> value="medium" /> 
						<label for="medium"><?php _e( 'Medium.', 'ilc' ); ?></label>
					</td>
					
					<td>
						<?php echo '<img src="' . plugins_url( 'small.gif' , __FILE__ ) . '" > '; ?><br/>
						<input id="small" name="<?php echo $name['size']; ?>" type="radio" <?php if ( $value['size'] == "small") echo 'checked="checked"'; ?> value="small" /> 
						<label for="small"><?php _e( 'Small.', 'ilc' ); ?></label>
					</td>
					
					<td>
						<?php echo '<img src="' . plugins_url( 'tall.gif' , __FILE__ ) . '" > '; ?><br/>
						<input id="tall" name="<?php echo $name['size']; ?>" type="radio" <?php if ( $value['size'] == "tall") echo 'checked="checked"'; ?> value="tall" /> 
						<label for="tall"><?php _e( 'Tall.', 'ilc' ); ?></label>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr>
		<th scope="row">
			<h3><?php _e('Button Placement in Posts/Pages:', 'ilc'); ?></h3>
		</th>
		<td >
			<table class="ilc-subtable">
				<tr>
					<td>
						<input id="before" name="<?php echo $name['place']; ?>" type="radio" <?php if ( $value['place'] == "before") echo 'checked="checked"'; ?> value="before" />
						<label for="before"><?php _e( 'Before content.', 'ilc' ); ?></label>
					</td>
		
					<td>
						<input id="after" name="<?php echo $name['place']; ?>" type="radio" <?php if ( $value['place'] == "after") echo 'checked="checked"'; ?> value="after" /> 
						<label for="after"><?php _e( 'After content.', 'ilc' ); ?></label>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr>
		<th scope="row">
			<h3><?php _e('Align left or right:', 'ilc'); ?></h3>
		</th>
		<td >
			<table class="ilc-subtable">
				<tr>
					<td>
						<input id="right" name="<?php echo $name['float']; ?>" type="radio" <?php if ( $value['float'] == "right") echo 'checked="checked"'; ?> value="right" />
						<label for="right"><?php _e( 'Align right.', 'ilc' ); ?></label>
					</td>
		
					<td>
						<input id="left" name="<?php echo $name['float']; ?>" type="radio" <?php if ( $value['float'] == "left") echo 'checked="checked"'; ?> value="left" /> 
						<label for="left"><?php _e( 'Align left.', 'ilc' ); ?></label>
					</td>
					
					<td>
						<input id="none" name="<?php echo $name['float']; ?>" type="radio" <?php if ( $value['float'] == "none") echo 'checked="checked"'; ?> value="none" /> 
						<label for="none"><?php _e( 'None.', 'ilc' ); ?></label>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
</table>


<p class="submit">
	<input type="submit" name="Submit" value="<?php _e('Update settings', 'ilc') ?>" class="button-primary" />
</p>
</form>

<style>
	.form-table table td{
		padding: 15px 40px 0 0;
	}
</style>

</div>

<?php
}
/*
 * Creates Settings link on plugins list page.
 */
function ilc_plus_one_settings($links, $file) {
	if ($file == plugin_basename(dirname(__FILE__).'/ilc-plus-one.php')) {
		foreach($links as $k=>$v) {
			if (strpos($v, 'plugin-editor.php?file=') !== false)
				unset($links[$k]);
		}
		$links[] = "<a href='options-general.php?page=".plugin_basename(dirname(__FILE__))."/ilc-admin.php'><b>Settings</b></a>";
	}
	return $links;
}

/*
 * Adds Settings link on plugins page. Create options page on wp-admin.
 */
function ilc_plus_one_plugin_menu() {
	add_filter( 'plugin_action_links', 'ilc_plus_one_settings', -10, 2);
	$page = add_options_page('ILC Plus One', 'ILC Plus One', 8, __FILE__, 'ilc_plus_one_options');
	
	add_action('admin_print_styles-' . $page, create_function("", "wp_enqueue_script( 'farbtastic' ); wp_enqueue_style( 'farbtastic' );"));
}
add_action('admin_menu', 'ilc_plus_one_plugin_menu');

/*
 * When the plugin is activated, we will setup some options on the database
 */
function ilc_plus_one_activate(){
	add_option("ilc_plus_one_path", WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__)));
	add_option('stored_ilc_plus_one', array(
		'lang' => '',
		'size' => '',
		'count' => true,
		'track' => true,
		'place' => 'after',
		'float' => 'none'
	));
}

/*
 * When the plugin is deactivated, we will erase all options from database
 */
function ilc_plus_one_deactivate(){
	delete_option('stored_ilc_plus_one', $stored_ilc_plus_one);
}

register_activation_hook(	dirname(__FILE__).'/ilc-plus-one.php', 'ilc_plus_one_activate');
register_deactivation_hook(	dirname(__FILE__).'/ilc-plus-one.php', 'ilc_plus_one_deactivate');
?>