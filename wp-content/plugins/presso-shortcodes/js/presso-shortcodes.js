/* -----------------------------------------------------------------------------
 * Document ready
 * -------------------------------------------------------------------------- */
;(function( $, window, document, undefined ){
	"use strict";
	
	$( document ).ready( function ($) {

		// -----------------------------------------------------------------------------
		// Accordion
		// 
		$( ".accordion" ).each( function( i, e ) {
			var $this = $( e );
			var options = {
				heightStyle: 'content',
				header: '.accordion-header',
				collapsible: true
			}

			if ( $this.data( 'open' ) == true ) {
				options.active = 0;
			} else {
				options.active = false;
			}

			$this.accordion( options );
		} );

		// -----------------------------------------------------------------------------
		// Tabs
		//
		$( '.tabs' ).each( function( i, el ) {
			var $tabs = $( el );
			$( '.tab-title', $tabs ).click( function( e ) {
				var $tab = $( this );
				var $content = $( '#tabpanel'+$tab.data( 'tab-id' ) );

				if ( $content.length ) {
					$( '.active', $tabs ).removeClass( 'active' );
					$( '.tab-content', $tabs ).hide();
					$content.show();
					$( '.tab-id-'+$tab.data( 'tab-id' ), $tabs ).addClass( 'active' );
				}
				
			} );

			$( '.tab-title', $tabs ).eq( 0 ).trigger( 'click' );
		} );

	} );
})( jQuery, window , document );